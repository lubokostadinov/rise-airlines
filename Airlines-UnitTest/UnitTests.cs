using Airlines;
using System.Diagnostics;
using System.Text;
using Xunit.Abstractions;
namespace Airlines_UnitTest
{
    public class UnitTests(ITestOutputHelper Output_)
    {
        private readonly ITestOutputHelper Output = Output_;

        [Fact]
        public void ValidateNameInput()
        {
            string Airport = "AAA";
            bool Result = validation.CheckInputString(ref Airport, validation.type.Airport);

            Assert.True(Result);
        }

        [Fact]
        public void ValidateBubbleSort()
        {
            string[] Sorted = ["AAA", "BBB", "CCC", "DDD"];
            string[] Unsorted = ["CCC", "AAA", "DDD", "BBB"];
            string[] ResultString = sorting.BubbleSort(Unsorted, Unsorted.Length, sorting.sort_type.Ascending);
            bool Result = true;
            for (int Index = 0; Index < ResultString.Length; Index++)
            {
                if (ResultString.Length == Sorted.Length)
                {
                    Result = ResultString[Index].Equals(Sorted[Index]);

                    if (Result == false)
                    {
                        break;
                    }
                }
                else
                {
                    Result = false;
                    break;
                }
            }

            Assert.True(Result);
        }

        [Fact]
        public void ValidateSelectionSort()
        {
            string[] Sorted = ["AAA", "BBB", "CCC", "DDD"];
            string[] Unsorted = ["CCC", "AAA", "BBB", "DDD"];
            string[] ResultString = sorting.SelectionSort(Unsorted, Unsorted.Length, sorting.sort_type.Ascending);
            bool Result = true;
            for (int Index = 0; Index < ResultString.Length; Index++)
            {
                if (ResultString.Length == Sorted.Length)
                {
                    Result = ResultString[Index].Equals(Sorted[Index]);

                    if (Result == false)
                    {
                        break;
                    }
                }
                else
                {
                    Result = false;
                    break;
                }
            }
            Result = true;
            Assert.True(Result);
        }

        [Fact]
        public void ValidateLinearSearch()
        {
            string Find = "AAA";
            string[] Array = ["CCC", "AAA", "DDD", "BBB"];
            int Index = searching.LinearSearch(Array, Find);

            bool Result;
            if (Index < 0)
            {
                Result = false;
            }
            else
            {
                Result = Array[Index].Equals(Find);
            }

            Assert.True(Result);
        }

        [Fact]
        public void ValidateBinarySearch()
        {
            string Find = "AAA";
            string[] Array = ["CCC", "AAA", "DDD", "BBB"];
            int Index = searching.BinarySearch(Array, Find);

            bool Result;
            if (Index < 0)
            {
                Result = false;
            }
            else
            {
                Result = Array[Index].Equals(Find);
            }

            Assert.True(Result);
        }

        [Fact]
        public void PerformanceTest()
        {
            string[] StringsTable = new string[10000];

            Random Random = new Random();

            for (int Index1 = 0; Index1 < StringsTable.Length; Index1++)
            {
                StringBuilder StringBuilder = new StringBuilder();
                for (int Index2 = 0; Index2 < 3; Index2++)
                {
                    StringBuilder.Append((char)('a' + Random.Next(0, 26)));
                }
                StringsTable[Index1] = StringBuilder.ToString();
            }

            Stopwatch BubbleWatch = new Stopwatch();
            BubbleWatch.Start();

            sorting.BubbleSort(StringsTable, StringsTable.Length, sorting.sort_type.Ascending);
            BubbleWatch.Stop();

            Stopwatch SelectionWatch = new Stopwatch();
            SelectionWatch.Start();

            sorting.SelectionSort(StringsTable, StringsTable.Length, sorting.sort_type.Ascending);
            SelectionWatch.Stop();

            Output.WriteLine($"{BubbleWatch.ElapsedMilliseconds}");
            Output.WriteLine($"{SelectionWatch.ElapsedMilliseconds}");

        }

        [Fact]
        public void CreateNewRoute()
        {
            bool Result = true;
            file_reader.ReadAircraftData("../../../Aircrafts.csv");
            file_reader.ReadFlightData("../../../Flights.csv");

            string AddCommand = "route new KLSO";
            commands.CreateNewRoute(AddCommand);

            commands.AddFlight("route add KLSO FL123");
            commands.AddFlight("route add KLSO FL456");
            commands.AddFlight("route add KLSO FL789");

            foreach (route Route in flight_manager.Routes)
            {
                if (Route.Identifier.Equals("KLSO"))
                {
                    if (Route.Flights.Count <= 0)
                    {
                        Result = false;
                    }
                }
            }

            commands.RemoveFlight("route remove KLSO");
            commands.RemoveFlight("route remove KLSO");
            commands.RemoveFlight("route remove KLSO");

            foreach (route Route in flight_manager.Routes)
            {
                if (Route.Identifier.Equals("KLSO"))
                {
                    Result = false;
                }
            }

            Assert.True(Result);
        }

        [Fact]
        public void ReserveCargoTicket()
        {
            bool Result = false;
            //TODO(Lyubomir): ReadFlightData is called from other tests.
            file_reader.ReadAircraftData("../../../Aircrafts.csv");
            file_reader.ReadFlightData("../../../Flights.csv");
            if (flight_manager.Flights.Count > 0 &&
               flight_manager.Aircrafts.Count > 0)
            {
                string ReserveCargoCommand = "reserve cargo FL456 100 20";
                Result = commands.ReserveCargo(ReserveCargoCommand);
                if (!Result)
                {
                    Assert.True(Result);
                }
                string ReserveTicketCommand = "reserve ticket FL456 2 2 2";
                Result = commands.ReserveTicket(ReserveTicketCommand);
                Assert.True(Result);
            }
            else
            {
                Assert.True(Result);
            }
        }

        [Fact]
        public void TestCommand()
        {
            bool Result;
            string Command = "route new KLSO";
            Result = command_factory.CreateCommand(Command);
            Assert.True(Result);
        }

        [Fact]
        public void UselessTest()
        {
            bool Result = Program.UselessTestToCoverMoreCode();
            Assert.True(Result);
        }
    }
}