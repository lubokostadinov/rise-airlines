﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Airlines
{
    public enum AircraftModel
    {
        Cargo,
        Passenger,
        Private
    }

    public class aircraft
    {
        public AircraftModel Model { get; set; }
        public string? Name { get; set; }
        public double CargoWeight { get; set; }
        public double CargoVolume { get; set; }
        public uint Seats { get; set; }
    }

    public class airport
    {
        public string? Identifier { get; set; }
        public string? Name { get; set; }
        public string? City { get; set; }
        public string? Country { get; set; }

        public List<route> Routes = [];

        public List<flight> Flights = [];

        public tree? AirportTree;
    }

    public class airline
    {
        public string? Name { get; set; }
    }

    public class flight
    {
        public string? Identifier { get; set; }
        public string? DepartureAirport { get; set; }
        public string? ArrivalAirport { get; set; }
        public aircraft? Aircraft { get; set; }
        public int TicketPrice { get; set; }
        public float HoursDuration { get; set; }
    }

    public class route
    {
        public string? Identifier { get; set; }
        public LinkedList<flight> Flights = [];
    }

    //TODO(Lyubomir): Make functions in the managers for initializing the data
    public class airport_manager
    {
        public static List<airport> Airports = [];

        public static List<string> AirportNames = [];

        public static Dictionary<string, List<airport>> AirportCityMap = [];

        public static Dictionary<string, List<airport>> AirportCountryMap = [];
    }

    public class airline_manager
    {
        public static List<airline> Airlines = [];

        public static HashSet<string> AirlineNamesSet = [];

        public static List<string> AirlineNames = [];
    }

    public class flight_manager
    {
        public static List<string> FlightNames = [];

        public static List<flight> Flights = [];

        public static List<route> Routes = [];

        public static List<aircraft> Aircrafts = [];
    }

    public class tree_node
    {
        public airport? Airport;
        public List<tree_node> Children = [];
    }

    public class tree
    {
        public airport? StartingAirport;
        public List<tree_node> Children = [];
    }
}