﻿namespace Airlines
{
    public static class validation
    {
        internal enum type
        {
            Airline,
            Airport,
            Flight
        }
        internal static bool CheckInputString(ref string Name, type type)
        {
            bool StringIsValid = true;

            switch (type)
            {
                case type.Airport:
                    StringIsValid = (Name.All(char.IsLetter) && Name.Length == 3);
                    break;

                case type.Airline:
                    StringIsValid = Name.Length < 6;
                    break;

                case type.Flight:
                    StringIsValid = Name.All(char.IsLetterOrDigit);
                    break;
                default:
                    break;
            }

            if (StringIsValid == false)
            {
                Console.WriteLine("Invalid Name! Try Again!");
                Name = Console.ReadLine();
                if (Name != null)
                {
                    StringIsValid = CheckInputString(ref Name, type);
                }
            }

            return StringIsValid;
        }

        internal static bool ValidateAirport(airport NewAirport)
        {
            if (NewAirport.Identifier.Length is < 2 or > 4)
            {
                throw new invalid_name_exception("InvalidName " + NewAirport.Identifier);
            }

            if (!NewAirport.Identifier.All(char.IsLetterOrDigit))
            {
                throw new invalid_name_exception("InvalidName " + NewAirport.Identifier);
            }

            foreach (char Char in NewAirport.Name)
            {
                if (!(char.IsLetter(Char) || char.IsWhiteSpace(Char)))
                {
                    throw new invalid_name_exception("InvalidName " + NewAirport.Identifier);
                }
            }

            foreach (char Char in NewAirport.City)
            {
                if (!(char.IsLetter(Char) || char.IsWhiteSpace(Char)))
                {
                    throw new invalid_name_exception("InvalidName " + NewAirport.Identifier);
                }
            }

            foreach (char Char in NewAirport.Country)
            {
                if (!(char.IsLetter(Char) || char.IsWhiteSpace(Char)))
                {
                    throw new invalid_name_exception("InvalidName " + NewAirport.Identifier);
                }
            }

            return true;
        }

        internal static bool ValidateFlightConnection(route Route)
        {
            bool Result = true;
            flight CurrentFlight;
            flight NextFlight;
            for (int Index = 0; Index < Route.Flights.Count; ++Index)
            {
                if (Route.Flights.ElementAt(Index) != Route.Flights.Last())
                {
                    CurrentFlight = Route.Flights.ElementAt(Index);
                    NextFlight = Route.Flights.ElementAt(Index + 1);
                    if (CurrentFlight.ArrivalAirport == NextFlight.DepartureAirport)
                    {
                        Result = true;
                    }
                    else
                    {
                        throw new invalid_flight_code_exception("InvalidFlightConnection");
                    }
                }
            }
            return Result;
        }
    }
}