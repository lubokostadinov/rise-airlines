﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines
{
    [Serializable]
    public class invalid_input_exception : Exception
    {
        public string? ValidInput { get; }

        public invalid_input_exception() { }

        public invalid_input_exception(string message)
            : base(message) { }

        public invalid_input_exception(string message, Exception inner)
            : base(message, inner) { }

        public invalid_input_exception(string message, string Input)
            : this(message) => ValidInput = Input;
    }

    [Serializable]
    public class invalid_flight_code_exception : Exception
    {
        public string? FlightMessage { get; }

        public invalid_flight_code_exception() { }

        public invalid_flight_code_exception(string message)
            : base(message) { }

        public invalid_flight_code_exception(string message, Exception inner)
            : base(message, inner) { }

        public invalid_flight_code_exception(string message, string Flight)
            : this(message) => FlightMessage = Flight;
    }

    [Serializable]
    public class invalid_name_exception : Exception
    {
        public string? AirlineMessage { get; }

        public invalid_name_exception() { }

        public invalid_name_exception(string message)
            : base(message) { }

        public invalid_name_exception(string message, Exception inner)
            : base(message, inner) { }

        public invalid_name_exception(string message, string Airline)
            : this(message) => AirlineMessage = Airline;
    }
}
