﻿namespace Airlines
{
    public static class sorting
    {
        internal enum sort_algorithm
        {
            Bubble,
            Selection,
        }
        internal enum sort_type
        {
            Ascending,
            Descending,
        }
        internal static string[] BubbleSort(string[] Array, int Size, sort_type type)
        {
            string Temp;
            for (int SortIndex = 0; SortIndex < Size; Size--)
            {
                for (int ArrayIndex = 0; ArrayIndex < Size - 1; ArrayIndex++)
                {
                    for (int StringIndex = 0; StringIndex < Array[ArrayIndex].Length; StringIndex++)
                    {
                        if (type == sort_type.Ascending)
                        {
                            if (Array[ArrayIndex][StringIndex] > Array[ArrayIndex + 1][StringIndex])
                            {
                                Temp = Array[ArrayIndex];
                                Array[ArrayIndex] = Array[ArrayIndex + 1];
                                Array[ArrayIndex + 1] = Temp;
                                break;
                            }
                        }
                        else if (type == sort_type.Descending)
                        {
                            if (Array[ArrayIndex][StringIndex] < Array[ArrayIndex + 1][StringIndex])
                            {
                                Temp = Array[ArrayIndex];
                                Array[ArrayIndex] = Array[ArrayIndex + 1];
                                Array[ArrayIndex + 1] = Temp;
                                break;
                            }
                        }
                        else
                        {
                            //NOTE(Lyubomir): Invalid Code Path!
                        }
                    }
                }
            }

            return Array;
        }

        internal static string[] SelectionSort(string[] Array, int Size, sort_type type)
        {
            for (int SortIndex = 0; SortIndex < Size; SortIndex++)
            {
                string MinString = Array[SortIndex];

                for (int StringIndex = SortIndex; StringIndex < Size; StringIndex++)
                {
                    for (int CharIndex = 0; CharIndex < Array[StringIndex].Length; CharIndex++)
                    {
                        if (type == sort_type.Ascending)
                        {
                            if (Array[StringIndex][CharIndex] < MinString[CharIndex])
                            {
                                Array[SortIndex] = Array[StringIndex];
                                Array[StringIndex] = MinString;
                                MinString = Array[SortIndex];
                                break;
                            }
                        }
                        else if (type == sort_type.Descending)
                        {
                            if (Array[StringIndex][CharIndex] > MinString[CharIndex])
                            {
                                Array[SortIndex] = Array[StringIndex];
                                Array[StringIndex] = MinString;
                                MinString = Array[SortIndex];
                                break;
                            }
                        }
                        else
                        {
                            //NOTE(Lyubomir): Invalid Code Path!
                        }
                    }
                }
            }
            return Array;
        }
    }
}