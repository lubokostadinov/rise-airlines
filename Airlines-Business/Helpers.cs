﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Airlines
{
    public static class helpers
    {
        internal static void CreateTree(ref airport RootAirport)
        {
            RootAirport.AirportTree = new tree { StartingAirport = RootAirport };
            for (int Index = 0; Index < RootAirport.Flights.Count; ++Index)
            {
                for (int AirportIndex = 0; AirportIndex < airport_manager.Airports.Count; ++AirportIndex)
                {
                    if (airport_manager.Airports[AirportIndex].Identifier == RootAirport.Flights[Index].ArrivalAirport)
                    {
                        RootAirport.AirportTree.Children.Add(new tree_node { Airport = airport_manager.Airports[AirportIndex] });
                        break;
                    }
                }
            }

            for (int Index = 0; Index < RootAirport.AirportTree.Children.Count; ++Index)
            {
                if (RootAirport.AirportTree.Children[Index].Airport.AirportTree is null)
                {
                    CreateTree(ref RootAirport.AirportTree.Children[Index].Airport);
                }
            }
        }

        private static int Depth;
        internal static route FindRoutes(tree Tree, string AirportIdentifier, ref List<route> Routes)
        {
            Depth++;
            route Route = new route { Flights = [] };

            if (Tree.StartingAirport.Identifier != AirportIdentifier)
            {
                for (int Index = 0; Index < Tree.Children.Count; ++Index)
                {
                    if (Tree.Children[Index].Airport.Identifier == AirportIdentifier)
                    {
                        for (int FlightIndex = 0; FlightIndex < Tree.StartingAirport.Flights.Count; ++FlightIndex)
                        {
                            if (Tree.StartingAirport.Flights[FlightIndex].ArrivalAirport == AirportIdentifier)
                            {
                                Route.Flights.AddLast(Tree.StartingAirport.Flights[FlightIndex]);
                                if (Depth == 1)
                                {
                                    Routes.Add(Route);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int FlightIndex = 0; FlightIndex < Tree.StartingAirport.Flights.Count; ++FlightIndex)
                        {
                            if (Tree.Children[Index].Airport.Identifier == Tree.StartingAirport.Flights[FlightIndex].ArrivalAirport)
                            {
                                Route.Flights.AddLast(Tree.StartingAirport.Flights[FlightIndex]);
                                tree ChildTree = Tree.Children[Index].Airport.AirportTree;
                                route ChildRoute = FindRoutes(ChildTree, AirportIdentifier, ref Routes);
                                Depth--;
                                foreach (flight Flight in ChildRoute.Flights)
                                {
                                    Route.Flights.AddLast(Flight);
                                }

                                if (Depth == 1)
                                {
                                    if (Route.Flights.Last().ArrivalAirport == AirportIdentifier)
                                    {
                                        route NewRoute = new route { Flights = [] };
                                        foreach (flight Flight in Route.Flights)
                                        {
                                            NewRoute.Flights.AddLast(Flight);
                                        }

                                        Routes.Add(NewRoute);
                                        Route.Flights.Clear();
                                    }
                                    else
                                    {
                                        Route.Flights.Clear();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Depth == 1)
            {
                Depth = 0;
            }
            return Route;
        }
    }
}