﻿using Airlines;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines
{
    public static class command_factory
    {
        public static bool CreateCommand(string Command)
        {
            if (Command.Contains(commands.SearchCommand))
            {
                search_command SearchCommand = new search_command(Command);
                SearchCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.SortCommand))
            {
                sort_command SortCommand = new sort_command(Command);
                SortCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.ExistCommand))
            {
                exist_command ExistCommand = new exist_command(Command);
                ExistCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.ListCommand))
            {
                list_command ListCommand = new list_command(Command);
                ListCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.NewRouteCommand))
            {
                new_route_command NewRouteCommand = new new_route_command(Command);
                NewRouteCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.AddFlightCommand))
            {
                add_flight_command AddFlightCommand = new add_flight_command(Command);
                AddFlightCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.RemoveFlightCommand))
            {
                remove_flight_command RemoveFlightCommand = new remove_flight_command(Command);
                RemoveFlightCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.PrintRouteCommand))
            {
                print_route_command PrintRouteCommand = new print_route_command(Command);
                PrintRouteCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.ReserveCargoCommand))
            {
                reserve_cargo_command ReserveCargoCommand = new reserve_cargo_command(Command);
                ReserveCargoCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.ReserveTicketCommand))
            {
                reserve_ticket_command ReserveTicketCommand = new reserve_ticket_command(Command);
                ReserveTicketCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.BatchCommand))
            {
                batch_command BatchCommand = new batch_command(Command);
                BatchCommand.Execute(Command);
                return true;
            }
            if (Command.Contains(commands.RouteFindCommand))
            {
                route_find_command RouteFindCommand = new route_find_command(Command);
                RouteFindCommand.Execute(Command);
                return true;
            }

            return false;
        }
    }

    public interface command_interface
    {
        void Execute(string Command);
    }

    public class search_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;

        public void Execute(string Command) => commands.Search(Command);
    }

    public class sort_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.Sort(Command);
    }

    public class exist_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.Exist(Command);
    }

    public class list_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.List(Command);
    }

    public class new_route_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.CreateNewRoute(Command);
    }

    public class add_flight_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.AddFlight(Command);
    }

    public class remove_flight_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.RemoveFlight(Command);
    }

    public class print_route_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.PrintRoute(Command);
    }

    public class reserve_cargo_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.ReserveCargo(Command);
    }

    public class reserve_ticket_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.ReserveTicket(Command);
    }

    public class batch_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.Batch(Command);
    }

    public class route_find_command(string Command) : command_interface
    {
        public string? Command { get; set; } = Command;
        public void Execute(string Command) => commands.FindRouteCommand(Command);
    }

    //TODO(Lyubomir): Maybe it makes more sense for commands to be functions and not objects?
    public static class commands
    {
        public static string LookupString = "";
        public static readonly string SearchCommand = "search ";
        public static readonly string SortCommand = "sort ";
        public static readonly string ExistCommand = "exist ";
        public static readonly string ListCommand = "list ";
        public static readonly string NewRouteCommand = "route new ";
        public static readonly string AddFlightCommand = "route add ";
        public static readonly string RemoveFlightCommand = "route remove ";
        public static readonly string PrintRouteCommand = " route print ";
        public static readonly string ReserveCargoCommand = "reserve cargo ";
        public static readonly string ReserveTicketCommand = "reserve ticket ";
        public static readonly string BatchCommand = "batch ";
        public static List<string> BatchCommands = [];
        public static readonly string RouteFindCommand = "route find ";
        public static readonly string RouteCheckCommand = "route check ";
        public static readonly string RouteSearchCommand = "route search ";

        internal static string[] Search(string Command)
        {
            string[] Result = ["", "", ""];

            if (Command.Contains(SearchCommand))
            {
                LookupString = Command[SearchCommand.Length..];
                int StringIndex = searching.BinarySearch([.. airport_manager.AirportNames], LookupString);
                if (StringIndex >= 0)
                {
                    Result[0] = airport_manager.AirportNames[StringIndex];
                }
                else
                {
                    Result[0] = "Airport Not Found!";
                }
                StringIndex = searching.BinarySearch([.. airline_manager.AirlineNames], LookupString);
                if (StringIndex >= 0)
                {
                    Result[1] = airline_manager.AirlineNames[StringIndex];
                }
                else
                {
                    Result[1] = "Airline Not Found!";
                }
                StringIndex = searching.BinarySearch([.. flight_manager.FlightNames], LookupString);
                if (StringIndex >= 0)
                {
                    Result[2] = flight_manager.FlightNames[StringIndex];
                }
                else
                {
                    Result[2] = "Flight Not Found!";
                }
            }
            else
            {
                Result = ["Invalid Command!"];
            }

            return Result;
        }

        internal static void Sort(string Command)
        {
            if (Command.Contains(SortCommand))
            {
                string sort_type = Command[SortCommand.Length..];
                if ((sort_type.Contains('<') && sort_type.Contains('>')) && (sort_type.Contains('[') && sort_type.Contains(']')))
                {
                    string Ordertype = sort_type[(sort_type.IndexOf('[') + 1)..sort_type.IndexOf(']')];
                    sort_type = sort_type[(sort_type.IndexOf('<') + 1)..sort_type.IndexOf('>')];
                    sorting.sort_type type = default;

                    if (Ordertype == "Ascending")
                    {
                        type = sorting.sort_type.Ascending;
                    }
                    else if (Ordertype == "Descending")
                    {
                        type = sorting.sort_type.Descending;
                    }

                    switch (sort_type)
                    {
                        case "Airports":
                            string[] Airports = sorting.BubbleSort([.. airport_manager.AirportNames], airport_manager.AirportNames.Count, type);
                            airport_manager.AirportNames = new List<string>(Airports);
                            break;
                        case "Airlines":
                            string[] Airlines = sorting.SelectionSort([.. airline_manager.AirlineNames], airline_manager.AirlineNames.Count, type);
                            airline_manager.AirlineNames = new List<string>(Airlines);
                            break;
                        case "Flights":
                            string[] Flights = sorting.SelectionSort([.. flight_manager.FlightNames], flight_manager.FlightNames.Count, type);
                            flight_manager.FlightNames = new List<string>(Flights);
                            break;
                        default:
                            Console.WriteLine("Something Doesn't exist!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Command!");
                }
            }
        }

        internal static bool Exist(string Command)
        {
            if (Command.Contains(ExistCommand))
            {
                string Name = Command[ExistCommand.Length..];

                if (airline_manager.AirlineNamesSet.Contains(Name))
                {
                    Console.WriteLine("Name exists");
                    return true;
                }
                else
                {
                    Console.WriteLine("Name not found");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Invalid Command!");
                return false;
            }
        }

        internal static void List(string Command)
        {
            if (Command.Contains(ListCommand))
            {
                string Name = Command[ListCommand.Length..];
                if (airport_manager.AirportCityMap.TryGetValue(Name, out var CityAirports))
                {
                    foreach (var Airport in CityAirports)
                    {
                        Console.WriteLine(Airport.Name);
                    }
                }
                else if (airport_manager.AirportCountryMap.TryGetValue(Name, out var CountryAirports))
                {
                    foreach (var Airport in CountryAirports)
                    {
                        Console.WriteLine(Airport.Name);
                    }
                }
                else
                {
                    Console.WriteLine("City/Country Not Found!");
                }
            }
            else
            {
                Console.WriteLine("Invalid Command!");
            }
        }

        internal static void CreateNewRoute(string Command)
        {
            if (Command.Contains(NewRouteCommand))
            {
                Command = Command[NewRouteCommand.Length..];
                route NewRoute = new route
                {
                    Identifier = Command
                };
                flight_manager.Routes.Add(NewRoute);
            }
        }

        internal static void AddFlight(string Command)
        {
            if (Command.Contains(AddFlightCommand))
            {
                Command = Command[NewRouteCommand.Length..];
                string[] Fields = Command.Split(' ');

                for (int Index = 0; Index < flight_manager.Routes.Count; ++Index)
                {
                    if (flight_manager.Routes[Index].Identifier.Equals(Fields[0]))
                    {
                        foreach (flight Flight in flight_manager.Flights)
                        {
                            if (Flight.Identifier.Equals(Fields[1]))
                            {
                                flight_manager.Routes[Index].Flights.AddLast(Flight);
                                bool Result = validation.ValidateFlightConnection(flight_manager.Routes[Index]);
                                if (Result == false)
                                {
                                    Console.WriteLine("Invalid Flight Connection!");
                                    flight_manager.Routes[Index].Flights.RemoveLast();
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                Console.WriteLine("Route Not Found!");
            }
        }

        internal static void RemoveFlight(string Command)
        {
            if (Command.Contains(RemoveFlightCommand))
            {
                Command = Command[RemoveFlightCommand.Length..];
                foreach (route Route in flight_manager.Routes)
                {
                    if (Route.Identifier == Command)
                    {
                        Route.Flights.RemoveLast();

                        if (Route.Flights.Count == 0)
                        {
                            flight_manager.Routes.Remove(Route);
                            break;
                        }
                    }
                }
            }
        }

        internal static void PrintRoute(string Command)
        {
            if (Command.Contains(PrintRouteCommand))
            {
                Command = Command[NewRouteCommand.Length..];
                foreach (route Route in flight_manager.Routes)
                {
                    if (Route.Identifier == Command)
                    {
                        foreach (flight Flight in Route.Flights)
                        {
                            Console.WriteLine(Flight.Identifier);
                        }
                    }
                }
            }
        }

        internal static bool ReserveCargo(string Command)
        {
            bool Result = false;
            if (Command.Contains(ReserveCargoCommand))
            {
                Command = Command[ReserveCargoCommand.Length..];
                string[] Fields = Command.Split(' ');

                for (int Index = 0; Index < flight_manager.Flights.Count; ++Index)
                {
                    if (flight_manager.Flights[Index].Identifier == Fields[0])
                    {
                        Result = true;
                        flight_manager.Flights[Index].Aircraft.CargoWeight -= Convert.ToDouble(Fields[1]);
                        flight_manager.Flights[Index].Aircraft.CargoVolume -= Convert.ToDouble(Fields[2]);
                        //TODO(Lyubomir): Maybe return conformation the cargo was reserved.
                    }
                }
            }
            return Result;
        }

        internal static bool ReserveTicket(string Command)
        {
            bool Result = false;
            if (Command.Contains(ReserveTicketCommand))
            {
                Command = Command[ReserveTicketCommand.Length..];
                string[] Fields = Command.Split(' ');

                for (int Index = 0; Index < flight_manager.Flights.Count; ++Index)
                {
                    if (flight_manager.Flights[Index].Identifier == Fields[0])
                    {
                        Result = true;
                        //TODO(Lyubomir): Check there is enough space for the reservation
                        flight_manager.Flights[Index].Aircraft.Seats -= Convert.ToUInt32(Fields[1]);
                        if (flight_manager.Flights[Index].Aircraft.Seats < 0)
                        {
                            flight_manager.Flights[Index].Aircraft.Seats += Convert.ToUInt32(Fields[1]);
                            Console.WriteLine("Not enough seats!");
                        }

                        uint SmallBaggageCount = Convert.ToUInt32(Fields[1]);
                        uint LargeBaggageCount = Convert.ToUInt32(Fields[2]);
                        if (SmallBaggageCount > 0)
                        {
                            flight_manager.Flights[Index].Aircraft.CargoWeight -= SmallBaggageCount * 15;
                            flight_manager.Flights[Index].Aircraft.CargoVolume -= SmallBaggageCount * 0.045;
                            if (flight_manager.Flights[Index].Aircraft.CargoWeight < 0 ||
                                flight_manager.Flights[Index].Aircraft.CargoVolume < 0)
                            {
                                flight_manager.Flights[Index].Aircraft.CargoWeight += SmallBaggageCount * 15;
                                flight_manager.Flights[Index].Aircraft.CargoVolume += SmallBaggageCount * 0.045;
                                Console.WriteLine("Not enough space for baggage!");
                            }
                        }
                        if (LargeBaggageCount > 0)
                        {
                            flight_manager.Flights[Index].Aircraft.CargoWeight -= LargeBaggageCount * 30;
                            flight_manager.Flights[Index].Aircraft.CargoVolume -= LargeBaggageCount * 0.09;
                            if (flight_manager.Flights[Index].Aircraft.CargoWeight < 0 ||
                                flight_manager.Flights[Index].Aircraft.CargoVolume < 0)
                            {
                                flight_manager.Flights[Index].Aircraft.CargoWeight -= LargeBaggageCount * 30;
                                flight_manager.Flights[Index].Aircraft.CargoVolume -= LargeBaggageCount * 0.09;
                                Console.WriteLine("Not enough space for baggage!");
                            }
                        }
                        //TODO(Lyubomir): Maybe return conformation the ticket was reserved.
                    }
                }
            }
            return Result;
        }

        internal static void Batch(string Command)
        {
            if (Command.Contains(BatchCommand))
            {
                string BatchCommand = "";
                while (BatchCommand != "cancel")
                {
                    Console.WriteLine("Enter Batch Command:");
                    BatchCommand = Console.ReadLine();
                    BatchCommands.Add(BatchCommand);

                    if (BatchCommand == "start")
                    {
                        for (int Index = 0; Index < BatchCommands.Count; ++Index)
                        {
                            command_factory.CreateCommand(BatchCommands[Index]);
                        }
                        break;
                    }
                }
            }
        }

        internal static List<route> FindRouteCommand(string Command)
        {
            List<route> Result = [];
            if (Command.Contains(RouteFindCommand))
            {
                Command = Command[RouteFindCommand.Length..];
                string[] Fields = Command.Split(' ');
                for (int Index = 0; Index < airport_manager.Airports.Count; ++Index)
                {
                    if (airport_manager.Airports[Index].Identifier == Fields[0])
                    {
                        for (int Index2 = 0; Index2 < airport_manager.Airports.Count; ++Index2)
                        {
                            if (airport_manager.Airports[Index2].Identifier == Fields[1])
                            {
                                airport Airport = airport_manager.Airports[Index];
                                airport Destination = airport_manager.Airports[Index2];
                                helpers.CreateTree(ref Airport);
                                route Route = helpers.FindRoutes(airport_manager.Airports[1].AirportTree, Destination.Identifier, ref Result);
                                return Result;
                            }
                        }
                        break;
                    }
                }
            }
            return Result;
        }

        internal static bool RouteCheck(string Command)
        {
            bool Result = false;

            if (Command.Contains(RouteCheckCommand))
            {
                Command = Command[RouteCheckCommand.Length..];
                string[] Fields = Command.Split(' ');

                List<route> Routes = FindRouteCommand(RouteFindCommand + Fields[0] + " " + Fields[1]);
                if (Routes.Count > 0)
                {
                    Result = true;
                }
            }

            return Result;
        }

        public enum search_strategy
        {
            Shortest,
            Cheapest,
            LeastStops
        }

        internal static route FindShortestRoute(List<route> Routes)
        {
            route Result = Routes[0];
            float ShortestDuration = 0;
            foreach (flight Flight in Routes[0].Flights)
            {
                ShortestDuration = Flight.HoursDuration + ShortestDuration;
            }

            for (int Index = 0; Index < Routes.Count; Index++)
            {
                float CurrentDuration = 0;
                foreach (flight Flight in Routes[Index].Flights)
                {
                    CurrentDuration = Flight.HoursDuration + CurrentDuration;
                }
                if (CurrentDuration < ShortestDuration)
                {
                    ShortestDuration = CurrentDuration;
                    Result = Routes[Index];
                }
            }

            return Result;
        }

        internal static route FindCheapestRoute(List<route> Routes)
        {
            route Result = Routes[0];
            float CheapestPrice = 0;
            foreach (flight Flight in Routes[0].Flights)
            {
                CheapestPrice = Flight.TicketPrice + CheapestPrice;
            }

            for (int Index = 0; Index < Routes.Count; Index++)
            {
                float CurrentPrice = 0;
                foreach (flight Flight in Routes[Index].Flights)
                {
                    CurrentPrice = Flight.TicketPrice + CurrentPrice;
                }

                if (CurrentPrice < CheapestPrice)
                {
                    CheapestPrice = CurrentPrice;
                    Result = Routes[Index];
                }
            }

            return Result;
        }

        internal static route FindLeastStopsRoute(List<route> Routes)
        {
            route Result = Routes[0];
            int LeastStops = Routes[0].Flights.Count;

            for (int Index = 0; Index < Routes.Count; Index++)
            {
                if (Routes[Index].Flights.Count < LeastStops)
                {
                    LeastStops = Routes[Index].Flights.Count;
                    Result = Routes[Index];
                }
            }

            return Result;
        }

        internal static route RouteSearch(string Command)
        {
            route Result = new route();

            if (Command.Contains(RouteSearchCommand))
            {
                Command = Command[RouteSearchCommand.Length..];
                string[] Fields = Command.Split(' ');

                List<route> Routes = FindRouteCommand(RouteFindCommand + Fields[0] + " " + Fields[1]);
                switch (Fields[2])
                {
                    case "short":
                        Result = FindShortestRoute(Routes);
                        break;
                    case "cheap":
                        Result = FindCheapestRoute(Routes);
                        break;
                    case "stop":
                        Result = FindLeastStopsRoute(Routes);
                        break;
                    default:
                        break;
                }
            }

            return Result;
        }
    }
}