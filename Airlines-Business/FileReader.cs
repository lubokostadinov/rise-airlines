﻿namespace Airlines
{
    public static class file_reader
    {
        internal static void DefineAircraftstype()
        {
            for (int Index = 0; Index < flight_manager.Aircrafts.Count; ++Index)
            {
                if (flight_manager.Aircrafts[Index].Seats <= 0)
                {
                    flight_manager.Aircrafts[Index].Model = AircraftModel.Cargo;
                }
                else if (flight_manager.Aircrafts[Index].CargoVolume <= 0 && flight_manager.Aircrafts[Index].CargoWeight <= 0)
                {
                    flight_manager.Aircrafts[Index].Model = AircraftModel.Private;
                }
                else if (flight_manager.Aircrafts[Index].CargoVolume > 0 &&
                         flight_manager.Aircrafts[Index].CargoWeight > 0 &&
                         flight_manager.Aircrafts[Index].Seats > 0)
                {
                    flight_manager.Aircrafts[Index].Model = AircraftModel.Passenger;
                }
                else
                {
                    throw new invalid_input_exception("InvalidInput " + flight_manager.Aircrafts[Index].Name);
                }
            }
        }

        internal static void ReadAircraftData(string FileName)
        {
            using (StreamReader Reader = File.OpenText(FileName))
            {
                string Line;
                while ((Line = Reader.ReadLine()) is not null)
                {
                    string[] Fields = Line.Split(',');

                    aircraft NewAircraft = new aircraft
                    {
                        Name = Fields[0],
                        //TODO(Lyubomir): Not reeding doubles correctly!
                        CargoWeight = Convert.ToDouble(Fields[1]),
                        CargoVolume = Convert.ToDouble(Fields[2]),
                        Seats = Convert.ToUInt32(Fields[3])
                    };

                    flight_manager.Aircrafts.Add(NewAircraft);
                }
            }

            DefineAircraftstype();
        }

        internal static void ReadAirlineData(string FileName)
        {
            using StreamReader Reader = File.OpenText(FileName);
            string Line;
            while ((Line = Reader.ReadLine()) is not null)
            {
                airline NewAirline = new airline
                {
                    Name = Line
                };
                string Name = NewAirline.Name;
                if (validation.CheckInputString(ref Name, validation.type.Airline))
                {
                    airline_manager.Airlines.Add(NewAirline);
                    airline_manager.AirlineNamesSet.Add(NewAirline.Name);
                }
                else
                {
                    Console.WriteLine("Invalid airline data: " + Line);
                    throw new invalid_input_exception("InvalidInput " + Line);
                }
            }
        }

        internal static void ReadAirportData(string FileName)
        {
            using (StreamReader Reader = File.OpenText(FileName))
            {
                string Line;
                while ((Line = Reader.ReadLine()) is not null)
                {
                    string[] Fields = Line.Split(',');

                    airport NewAirport = new airport
                    {
                        Identifier = Fields[0],
                        Name = Fields[1],
                        City = Fields[2],
                        Country = Fields[3]
                    };

                    if (validation.ValidateAirport(NewAirport))
                    {
                        airport_manager.Airports.Add(NewAirport);
                        airport_manager.AirportNames.Add(NewAirport.Name);
                    }
                    else
                    {
                        Console.WriteLine("Invalid airport data: " + Line);
                    }
                }
            }

            HashSet<string> CityNames = [];
            foreach (airport Airport in airport_manager.Airports)
            {
                CityNames.Add(Airport.City);
            }

            foreach (string City in CityNames)
            {
                List<airport> CityAirports = [];
                foreach (airport Airport in airport_manager.Airports)
                {
                    if (Airport.City == City)
                    {
                        CityAirports.Add(Airport);
                    }
                }
                airport_manager.AirportCityMap.Add(City, CityAirports);
            }

            HashSet<string> CountryNames = [];
            foreach (airport Airport in airport_manager.Airports)
            {
                CountryNames.Add(Airport.Country);
            }

            foreach (string Country in CountryNames)
            {
                List<airport> CountryAirports = [];
                foreach (airport Airport in airport_manager.Airports)
                {
                    if (Airport.Country == Country)
                    {
                        CountryAirports.Add(Airport);
                    }
                }
                airport_manager.AirportCountryMap.Add(Country, CountryAirports);
            }
        }

        internal static void ReadFlightData(string FileName)
        {
            using StreamReader Reader = File.OpenText(FileName);
            string Line;
            while ((Line = Reader.ReadLine()) is not null)
            {
                string[] Fields = Line.Split(',');

                int AircraftIndex = -1;

                for (int Index = 0; Index < flight_manager.Aircrafts.Count; ++Index)
                {
                    if (flight_manager.Aircrafts[Index].Name == Fields[3])
                    {
                        AircraftIndex = Index;
                        break;
                    }
                }

                flight NewFlight = new flight
                {
                    Identifier = Fields[0],
                    DepartureAirport = Fields[1],
                    ArrivalAirport = Fields[2],
                    TicketPrice = Convert.ToInt32(Fields[4]),
                    HoursDuration = Convert.ToSingle(Fields[5]),
                    Aircraft = flight_manager.Aircrafts[AircraftIndex]
                };

                string Name = NewFlight.Identifier;
                string Departure = NewFlight.DepartureAirport;
                string Arrival = NewFlight.ArrivalAirport;
                if (validation.CheckInputString(ref Name, validation.type.Flight) &&
                    validation.CheckInputString(ref Departure, validation.type.Flight) &&
                    validation.CheckInputString(ref Arrival, validation.type.Flight))
                {
                    flight_manager.Flights.Add(NewFlight);
                    flight_manager.FlightNames.Add(NewFlight.Identifier);

                    for (int Index = 0; Index < airport_manager.Airports.Count; ++Index)
                    {
                        if (airport_manager.Airports[Index].Identifier == NewFlight.DepartureAirport)
                        {
                            airport_manager.Airports[Index].Flights.Add(NewFlight);
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Invalid flight data: " + Line);
                    throw new invalid_input_exception("InvalidInput " + Line);
                }
            }
        }

        internal static void ReadRouteData(string FileName)
        {
            using StreamReader Reader = File.OpenText(FileName);
            string Line;
            int FirstRow = 0;
            int AirportIndex = -1;
            while ((Line = Reader.ReadLine()) is not null)
            {
                if (FirstRow == 0)
                {
                    FirstRow++;
                    for (int Index = 0; Index < airport_manager.Airports.Count; ++Index)
                    {
                        if (validation.CheckInputString(ref Line, validation.type.Airport))
                        {
                            if (airport_manager.Airports[Index].Identifier == Line)
                            {
                                AirportIndex = Index;
                                airport_manager.Airports[Index].Routes.Add(new route { Identifier = Line });
                                flight_manager.Routes.Add(airport_manager.Airports[Index].Routes.Last());
                                break;
                            }
                        }
                    }
                }
                else
                {
                    for (int Index = 0; Index < flight_manager.Flights.Count; ++Index)
                    {
                        if (validation.CheckInputString(ref Line, validation.type.Flight))
                        {
                            if (flight_manager.Flights[Index].Identifier == Line)
                            {
                                airport_manager.Airports[AirportIndex].Routes.Last().Flights.AddLast(flight_manager.Flights[Index]);
                                flight_manager.Routes.Last().Flights.AddLast(flight_manager.Flights[Index]);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}