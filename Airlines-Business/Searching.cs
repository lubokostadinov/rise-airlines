﻿namespace Airlines
{
    public static class searching
    {
        internal static int LinearSearch(string[] Array, string Input)
        {
            for (int Index = 0; Index < Array.Length; Index++)
            {
                if (Array[Index] == Input)
                {
                    return Index;
                }
            }
            return -1;
        }

        internal static int BinarySearch(string[] Array, string Input)
        {
            int Left = 0, Right = Array.Length - 1;

            while (Left <= Right)
            {
                int Mid = Left + ((Right - Left) / 2);

                int Res = Input.CompareTo(Array[Mid]);

                if (Res == 0)
                    return Mid;

                if (Res > 0)
                    Left = Mid + 1;

                else
                    Right = Mid - 1;
            }
            return -1;
        }
    }
}