-- Create Airports table
CREATE TABLE Airports (
    AirportID INT PRIMARY KEY IDENTITY,
    Name VARCHAR(100) NOT NULL,
    Country VARCHAR(50) NOT NULL,
    City VARCHAR(50) NOT NULL,
    Code VARCHAR(3) NOT NULL,
    RunwaysCount INT NOT NULL,
    Founded DATE
);

-- Create Airlines table
CREATE TABLE Airlines (
    AirlineID INT PRIMARY KEY IDENTITY,
    Name VARCHAR(100) NOT NULL,
    Founded DATE,
    FleetSize INT,
    Description TEXT
);

-- Create Flights table
CREATE TABLE Flights (
    FlightID INT PRIMARY KEY IDENTITY,
    FlightNumber VARCHAR(20) NOT NULL,
    DepartureAirportID INT,
    ArrivalAirportID INT,
    DepartureDateTime DATETIME NOT NULL,
    ArrivalDateTime DATETIME NOT NULL,
);

-- Insert data for 3 airports
INSERT INTO Airports (Name, Country, City, Code, RunwaysCount, Founded)
VALUES 
('Airport1', 'Country1', 'CityA', 'AAA', 2, '2000-01-01'),
('Airport2', 'Country2', 'CityB', 'BBB', 3, '1985-05-12'),
('Airport3', 'Country3', 'CityC', 'CCC', 4, '1985-05-12');

-- Insert data for 2 flights
INSERT INTO Flights (FlightNumber, DepartureAirportID, ArrivalAirportID, DepartureDateTime, ArrivalDateTime)
VALUES 
('ABC123', 1, 2, '2024-04-15 08:00:00', '2024-04-15 10:00:00'),
('XYZ456', 2, 1, '2024-04-15 12:00:00', '2024-04-15 14:00:00');

-- Select flights departing from 'CityA'
SELECT * FROM Flights
WHERE DepartureAirportID IN (SELECT AirportID FROM Airports WHERE City = 'CityA');

-- Update final destination for flight 'ABC123'
UPDATE Flights
SET ArrivalAirportID = 3
WHERE FlightNumber = 'ABC123';

-- Delete data for flight 'XYZ456'
DELETE FROM Flights
WHERE FlightNumber = 'XYZ456';