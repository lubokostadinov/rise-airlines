Use [Rise Airlines]

-- Create Airports table
CREATE TABLE Airports (
    AirportID INT PRIMARY KEY IDENTITY,
    Name VARCHAR(100) NOT NULL,
    Country VARCHAR(50) NOT NULL,
    City VARCHAR(50) NOT NULL,
    Code VARCHAR(3) NOT NULL,
    RunwaysCount INT NOT NULL,
    Founded DATE
);

-- Create Airlines table
CREATE TABLE Airlines (
    AirlineID INT PRIMARY KEY IDENTITY,
    Name VARCHAR(100) NOT NULL,
    Founded DATE,
    FleetSize INT,
    Description TEXT
);

-- Create Flights table
CREATE TABLE Flights (
    FlightID INT PRIMARY KEY IDENTITY,
    FlightNumber VARCHAR(20) NOT NULL,
    DepartureAirportID INT,
    ArrivalAirportID INT,
    DepartureDateTime DATETIME NOT NULL,
    ArrivalDateTime DATETIME NOT NULL,
    AirlineID INT,
    FOREIGN KEY (DepartureAirportID) REFERENCES Airports(AirportID),
    FOREIGN KEY (ArrivalAirportID) REFERENCES Airports(AirportID),
    FOREIGN KEY (AirlineID) REFERENCES Airlines(AirlineID),
    CONSTRAINT CHK_FlightTimes CHECK (DepartureDateTime > GETDATE() AND ArrivalDateTime > GETDATE()),
    CONSTRAINT CHK_TimeOrder CHECK (DepartureDateTime < ArrivalDateTime)
);

-- Create indeces for Airports, Airlines and Flights data
CREATE INDEX idx_Airports_Code ON Airports (Code);
CREATE INDEX idx_Airports_Name ON Airports (Name);

CREATE INDEX idx_Airlines_Name ON Airlines (Name);

CREATE INDEX idx_Flights_DepartureAirportID ON Flights (DepartureAirportID);
CREATE INDEX idx_Flights_ArrivalAirportID ON Flights (ArrivalAirportID);
CREATE INDEX idx_Flights_AirlineID ON Flights (AirlineID);
CREATE INDEX idx_Flights_DepartureDateTime ON Flights (DepartureDateTime);
CREATE INDEX idx_Flights_ArrivalDateTime ON Flights (ArrivalDateTime);